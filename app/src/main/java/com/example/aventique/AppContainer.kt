package com.example.aventique

import android.app.Application
import com.example.aventique.data.ProductRepository
import com.example.aventique.ui.ProductViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers


class AppContainer(private val appContext: Application) {


    interface Factory<T> {
        fun create(): T
    }


    private val defaultDispatcher : CoroutineDispatcher = Dispatchers.IO

    private val productRepository  = ProductRepository()


    class ProductViewModelFactory(private val repo : ProductRepository) : Factory<ProductViewModel> {
        override fun create(): ProductViewModel {
            return ProductViewModel(repo)
        }
    }


    val productViewModelFactory = ProductViewModelFactory(productRepository)






}