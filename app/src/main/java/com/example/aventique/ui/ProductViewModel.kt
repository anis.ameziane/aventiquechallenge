package com.example.aventique.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.aventique.data.ProductModel
import com.example.aventique.data.ProductRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import com.example.aventique.data.Result

class ProductViewModel(private val productRepository: ProductRepository) : ViewModel() {

    private val _productsResult = MutableStateFlow<Result<List<ProductModel>>>(Result.Idle)
    val productsResult: StateFlow<Result<List<ProductModel>>> = _productsResult

    init {
        getProducts()
    }



    private fun getProducts() {
        viewModelScope.launch {
            _productsResult.value = productRepository.fetchProducts()
        }
    }

}


sealed class NetworkResult<T : Any> {
    class Success<T : Any>(val data: T?) : NetworkResult<T>()
    class Error<T : Any>(val code: Int, val message: String?) : NetworkResult<T>()
    class Exception<T : Any>(val e: Throwable) : NetworkResult<T>()
}




