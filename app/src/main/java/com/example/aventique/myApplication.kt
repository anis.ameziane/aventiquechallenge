package com.example.aventique

import android.app.Application
class MyApplication : Application() {
    val appContainer = AppContainer(appContext = this)
}