package com.example.aventique

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Card
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.ColorMatrix
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.example.aventique.data.ProductModel
import com.example.aventique.ui.theme.AventiqueTheme
import com.example.aventique.data.Result
import com.example.aventique.ui.ProductViewModel

class MainActivity : ComponentActivity() {
    private lateinit var viewModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val appContainer = (application as MyApplication).appContainer
        viewModel = appContainer.productViewModelFactory.create()
        setContent {
            AventiqueTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    navHostApp(viewModel = viewModel)
                }
            }
        }
    }
}


@Composable
fun navHostApp(
    navController: NavHostController = rememberNavController(),
    modifier: Modifier = Modifier,
    startDestination: String = "products",
    viewModel: ProductViewModel
) {
    NavHost(navController = navController, startDestination = startDestination) {
        composable(route = "products") {
            ProductScreen(viewModel, goToDetails = { product ->
                navController.currentBackStackEntry?.savedStateHandle?.set(
                    key = "product",
                    value = product
                )
                navController.navigate("productsDetails")
            })
        }
        composable(route = "productsDetails") {
            val product =
                navController.previousBackStackEntry?.savedStateHandle?.get<ProductModel>("product")

            product?.let {
                Log.d(
                    "productName",
                    product.toString()
                )
                ProductDetailsScreen(product, goBack = { navController.navigateUp() })
            }

        }

    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductDetailsScreen(product: ProductModel, goBack: () -> Unit) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        TopAppBar(
            title = { Text(text = product.productName) },
            navigationIcon = {
                IconButton(onClick = goBack) {
                    Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Back")
                }
            },
            modifier = Modifier
                .fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(8.dp))
        product.imageUrl?.largeImage?.takeIf { it.isNotEmpty() }?.let { largeImage ->
            AsyncImage(
                model = largeImage,
                contentDescription = product.productName,
            )
        }





        Spacer(modifier = Modifier.height(16.dp))

        Text(
            text = product.description ?: "No description available",
            style = TextStyle(fontSize = 18.sp)
        )

        Spacer(modifier = Modifier.height(8.dp))

        Text(
            text = "Price: $${product.price}",
            style = TextStyle(fontSize = 20.sp, fontWeight = FontWeight.Bold)
        )

        Spacer(modifier = Modifier.height(8.dp))

        Text(
            text = "Brand: ${product.brand.brandName}",
            style = TextStyle(fontSize = 18.sp, fontStyle = FontStyle.Italic)
        )
    }
}

@Composable
fun ProductScreen(
    productViewModel: ProductViewModel,
    goToDetails: (product: ProductModel) -> Unit
) {
    val productsResult by productViewModel.productsResult.collectAsState()

    when (productsResult) {
        is Result.Success -> {
            val products = (productsResult as Result.Success).data
            ProductList(products, goToDetails)
        }

        is Result.Error -> {
            val errorMessage = (productsResult as Result.Error).message
            Text(text = "Error: $errorMessage")
        }

        is Result.Loading -> {
            CircularProgressIndicator()
        }

        else -> {
        }
    }
}

@Composable
fun ProductList(products: List<ProductModel>, goToDetails: (product: ProductModel) -> Unit) {
    LazyColumn {
        items(products) { product ->
            ProductCard(product, goToDetails = goToDetails)
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ProductCard(product: ProductModel, goToDetails: (product: ProductModel) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(16.dp),
        onClick = { goToDetails(product) }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(16.dp)
        ) {
            AsyncImage(
                model = product.imageUrl?.smallImage,
                placeholder = painterResource(R.drawable.ic_launcher_foreground),
                contentDescription = product.productName,

                )

            Spacer(modifier = Modifier.height(16.dp))

            Text(
                text = product.productName,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.Bold
                )
            )

            Spacer(modifier = Modifier.height(8.dp))

            Text(
                text = product.description ?: "No description available",
                style = TextStyle(fontSize = 16.sp)
            )

            Spacer(modifier = Modifier.height(8.dp))


            Text(
                text = "Price: $${product.price}",
                style = TextStyle(fontSize = 18.sp, fontWeight = FontWeight.Bold)
            )

            Spacer(modifier = Modifier.height(8.dp))


            Text(
                text = "Brand: ${product.brand.brandName}",
                style = TextStyle(fontSize = 16.sp, fontStyle = FontStyle.Italic)
            )
        }
    }
}

