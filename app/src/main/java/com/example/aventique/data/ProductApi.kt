package com.example.aventique.data

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

interface ProductService {
    @GET("items.json")
    fun getProducts(): Call<List<ProductModel>>
}

object RetrofitClient {
    private const val BASE_URL = "https://sephoraios.github.io/"

    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
    val productService: ProductService = retrofit.create(ProductService::class.java)
}



