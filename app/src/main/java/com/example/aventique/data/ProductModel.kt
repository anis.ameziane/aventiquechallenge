package com.example.aventique.data
import android.os.Bundle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavOptionsBuilder
import com.google.gson.annotations.SerializedName

import java.io.Serializable

data class ProductModel(
    @SerializedName("product_id") val productId : Int,
    @SerializedName("product_name") val productName : String,
    @SerializedName("description") val description : String?,
    @SerializedName("price") val price :  Int,
    @SerializedName("images_url") val imageUrl  : ImageModel?,
    @SerializedName("c_brand") val brand : BrandModel,
    @SerializedName("is_productSet") val isProductSet :  Boolean?,
    @SerializedName("is_special_brand") val isSpecialBrand : Boolean?
) : Serializable

data class ImageModel(
    @SerializedName("small") val smallImage : String ,
    @SerializedName("large")  val largeImage  : String,
)

data class BrandModel(
   @SerializedName("id") val brandId:  String ,
   @SerializedName("name") val brandName: String
)




