package com.example.aventique.data

import com.example.aventique.data.RetrofitClient.productService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import java.io.IOException
class ProductRepository() {

    suspend fun fetchProducts(): Result<List<ProductModel>> {
        return try {
            val response = withContext(Dispatchers.IO) {
                productService.getProducts().execute()
            }

            if (response.isSuccessful) {
                val products = response.body()
                if (products != null) {
                    println("this is products ${products[0]}")
                    Result.Success(products)
                } else {
                    println("empty response")
                    Result.Error("Empty response body")
                }
            } else {
                println("failed to fetch")
                Result.Error("Failed to fetch products: ${response.code()}")
            }
        } catch (e: IOException) {
            println("failed networkError ${e.message}")
            Result.Error("Network error: ${e.message}")
        } catch (e: Exception) {
            println("unexpected ahh khraa $e")
            Result.Error("Unexpected error: ${e.message}")
        }
    }
}

sealed class Result<out T> {
    data class Success<out T>(val data: T) : Result<T>()
    data class Error(val message: String) : Result<Nothing>()
    object Loading : Result<Nothing>()
    object Idle : Result<Nothing>()
}

